import logo from './logo.svg';
import React, { Component } from 'react';
import Stepper from 'react-stepper-horizontal';
import './App.css';
import { MDBContainer, MDBRow, MDBCol, MDBStepper, MDBStep, MDBBtn, MDBInput } from "mdbreact";


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users2: [],
      error: null

    };
    
  }
  
  componentDidMount() {


    
    fetch('http://localhost:3002/api/user/1')
      .then(response => response.json())
      .then((data) => this.setState({ 
        users2: data.tenders}));
      //console.log('Colors Data: ',data.users)
      
  }

  renderTableHeader() {
   // let header = Object.keys(this.state.user2[0])
   // return header.map((key, index) => {
     //  return <th key={index}>{key.toUpperCase()}</th>
    //})
 }
 renderTableData() {
  return this.state.users2.map((student, index) => {
     const { userName,userId } = student //destructuring
     return (
        <tr key={userId}>
           <td>{userName}</td>
          
        </tr>
     )
  })
}
 render() {
  const  tenders  = this.state.users2;

    return (

      
       <div >
          <h1 id='title'>Etender Project</h1>
           <table id='tender'  className="table table-striped">
                <tbody>
                <h2 className='list-head'>TenderList</h2>
                <button onClick={this.handleaddClick}>
                  Add </button>
                 <button onClick={this.handleUpdateClick}>
                Update
                 </button>
                <tr><th><td>TenderRefNo</td><td>Document_Description</td><td>Location</td><td>Product_Catagory</td><td>DocumentDate</td></th></tr>
                  {tenders.map((user) => {
                    const { TenderRefNo,id,Document_Description,Location,Product_Catagory,Document_Upoladed_Date } = user;
               
        return (
            <table><tbody><tr><td>
              <span className='repo-text'>{user.TenderRefNo} </span>
              <span className='repo-text'>{user.Document_Description} </span>
              <span className='repo-text'>{user.Location} </span>
              <span className='repo-text'>{user.Product_Catagory} </span>
              <span className='repo-text'>{user.Document_Upoladed_Date} </span>
             </td></tr></tbody></table>

        );
      })}
             </tbody>
          </table>

       </div>
       
    )
 }
}
