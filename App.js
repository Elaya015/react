/* eslint no-unused-vars: 0 */
/* eslint no-console: 0 */
/* eslint space-infix-ops: 0 */
/* eslint max-len: 0 */
import React from 'react';
import { BootstrapTable, TableHeaderColumn ,DeleteButton,createCustomModalFooter,InsertModalHeader,closeModal} from 'react-bootstrap-table';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Modal, Button } from 'react-bootstrap';

function onRowSelect(row, isSelected) {
  console.log(row);
  console.log(`selected: ${isSelected}`);
}

function onSelectAll(isSelected) {
  console.log(`is select all: ${isSelected}`);
}

function onAfterSaveCell(row, cellName, cellValue) {
  console.log(`Save cell ${cellName} with value ${cellValue}`);
  console.log('The whole row :');
  console.log(row);
}

function onAfterTableComplete() {
  console.log('Table render complete.');
}

function onAfterDeleteRow(rowKeys) {
  console.log('onAfterDeleteRow');
  console.log(rowKeys);
}

function onAfterInsertRow(row) {
  console.log('onAfterInsertRow');
  console.log(row);
}

const selectRowProp = {
  mode: 'checkbox',
  clickToSelect: true,
  selected: [], // default select on table
  bgColor: 'rgb(238, 193, 213)',
  onSelect: onRowSelect,
  onSelectAll: onSelectAll
};

const cellEditProp = {
  mode: 'click',
  blurToSave: true,
  afterSaveCell: onAfterSaveCell
};

function confirmDelete(next,dropRowKeys){
   
}

handleChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit = (event) => {
    alert('A form was submitted: ' + this.state.TenderRefNo);

    var data = new FormData();
    const payload = {
      TenderRefNo: this.state.TenderRefNo,
      TenderType: this.state.TenderType,
      No_of_covers: this.state.No_of_covers,
      PaymentMode: this.state.PaymentMode,
      Document_Description: this.state.Document_Description,
      Document_Type: this.state.Document_Type,
      Document_Upoladed_Date: this.state.Document_Upoladed_Date,
      Product_Catagory: this.state.Product_Catagory,
      Product_SubCatagory: this.state.Product_SubCatagory,
      Location: this.state.Location,
      Tender_Value: this.state.Tender_Value,
      Delivery_Period: this.state.Delivery_Period,
      userId: 1,
      
    };
    

    fetch('http://localhost:3002/api/tender/2', {
        method: 'post',
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body:JSON.stringify(payload),
      }).then(function(response) {
        console.log(response)
        return response.json();
      });

    event.preventDefault();

    }


const options = {
  paginationShowsTotal: true,
  sortName: 'Product_Catagory',  // default sort column name
  sortOrder: 'desc',  // default sort order
  afterTableComplete: onAfterTableComplete, // A hook for after table render complete.
  afterDeleteRow: onAfterDeleteRow,  // A hook for after droping rows.
  afterInsertRow: onAfterInsertRow ,
  //insertModal: model,
  handleConfirmDeleteRow: deleteRow
  
 
};

function model(close,save){
    console.log("hello");

    return (
        <div>
          
          <div>
            <Modal className="modal-container" 
             // show={this.state.showModal} 
             // onHide={this.close}
              animation={true} 
              bsSize="small">
    
              <Modal.Header closeButton>
                <Modal.Title>Modal title</Modal.Title>
              </Modal.Header>
    
              <Modal.Body>
              <form >
        <label>
        TenderRefNo:
          <input type="text"  name="TenderRefNo"  />
        </label><br/>
        <label>
        TenderType:
          <input type="text"  name="TenderType" />
        </label><br/>
        <label>
        No_of_covers:
          <input type="text" name="No_of_covers"  />
        </label><br/>
        <label>
        PaymentMode:
          <input type="text"  name="PaymentMode" />
        </label><br/>
        <label>
        Document_Description:
          <input type="text"  name="Document_Description" />
        </label><br/>
        <label>
        Document_Type:
          <input type="text"  name="Document_Type" />
        </label><br/>
        <label>
        Document_Upoladed_Date:
          <input type="text" name="Document_Upoladed_Date"  />
        </label><br/>
        <label>
        Product_Catagory:
          <input type="text" name="Product_Catagory" />
        </label><br/>
        <label>
        Product_SubCatagory:
          <input type="text"  name="Product_SubCatagory" />
        </label><br/>
        <label>
        Location:
          <input type="text"  name="Location" />
        </label><br/>
        <label>
        Tender_Value:
          <input type="text" name="Tender_Value"  />
        </label><br/>
        <label>
        Delivery_Period:
          <input type="text" name="Delivery_Period"  />
        </label><br/>
        <input type="submit" value="Submit" />
      </form>

              </Modal.Body>
    
              <Modal.Footer>
                <Button onClick={close}>Close</Button>
                <Button bsStyle="primary">Save changes</Button>
              </Modal.Footer>         
            </Modal> 
          </div>
        </div>
      );

}
  
 function handleModalClose(closeModal) {
    // Custom your onCloseModal event here,
    // it's not necessary to implement this function if you have no any process before modal close
    console.log('This is my custom function for modal close event');
    closeModal();
  }
function priorityFormatter(cell, row) {
  if (cell === 'A') return '<font color="red">' + cell + '</font>';
  else if (cell === 'B') return '<font color="orange">' + cell + '</font>';
  else return cell;
}

function trClassNameFormat(rowData, rIndex) {
  return rIndex % 1 === 0 ? 'third-tr' : '';
}
function nameValidator(value) {
  if (!value) {
    return 'Field value is required!';
  } else if (value.length < 3) {
    return 'Job Name length must great 3 char';
  }
  return true;
}
function priorityValidator(value) {
  if (!value) {
    return 'Priority is required!';
  }
  return true;
}
function insertRow(){
    console.log("insertRow");
}

function deleteRow(row,rIndex){
    console.log('delete', rIndex);
    fetch('http://localhost:3002/api/tender' + "/" + rIndex, {
        method: 'DELETE'
      }).then(() => {
         console.log('removed');
      }).catch(err => {
        console.error(err)
      });
}
function open() {
   // showModal: true;
   // this.setState({showModal: true});
  }
  
 function close() {
    //this.setState({showModal: false});
    //showModal: false;
  }
  
export default class App extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
          users2: [],
          error: null,
          showModal: false
        };
       
        //  this.open = this.open.bind(this);
         // this.close = this.close.bind(this);
      }
     componentDidMount() {
     fetch('http://localhost:3002/api/user/1')
          .then(response => response.json())
          .then((data) => this.setState({ 
            users2: data.tenders}));
      

      }
  render() {
    const  tenders  = this.state.users2;
    {tenders.map((user) => {
        const { TenderRefNo,id,Document_Description,Location,Product_Catagory,Document_Upoladed_Date } = user;
        
    } );
 
  
}
    return (
      <BootstrapTable data={ this.state.users2 }
        trClassName={ trClassNameFormat }
        selectRow={ selectRowProp }
        cellEdit={ cellEditProp }
        options={ options }
        insertRow
        //insertModalFooter={model}
        deleteRow={deleteRow}
        search
        columnFilter
        hover
        pagination>
        <TableHeaderColumn dataField='id' dataAlign='center' dataSort isKey autoValue>Tender ID</TableHeaderColumn>
        <TableHeaderColumn dataField='TenderRefNo' className='good' dataSort
          editable={ { type: 'textarea' } }>TenderRefNo</TableHeaderColumn>
        
        <TableHeaderColumn dataField='Location'
          editable={ { type: 'text' } }>Location</TableHeaderColumn>
           <TableHeaderColumn dataField='Product_Catagory' className='good' dataSort
          editable={ { type: 'textarea' } }>Product_Catagory</TableHeaderColumn>
           <TableHeaderColumn dataField='Document_Upoladed_Date' className='good' dataSort
          editable={ { type: 'textarea' } }>Document_Upoladed_Date</TableHeaderColumn>
         
      </BootstrapTable>
    );
  }
}
