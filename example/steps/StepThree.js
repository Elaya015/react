import React from 'react';

const StepThree = () => {
		return (
						<section>
								<div className="row">
										<div className="col-md-6">
												<div className="form-group">
														<label htmlFor="wint1">Work/Item Title :</label>
														<input type="text" className="form-control required"
														       id="wint1" />
												</div>
														<div className="form-group">
																<label htmlFor="shortDescription3">Work/Item  Description :</label>
																<textarea name="document_description"
																          rows="3" className="form-control" 
																           />
														</div>
												<div className="form-group">
														<label htmlFor="wintType1">Product Catagory :</label>
														<select className="custom-select form-control required"
														        id="wintType1" data-placeholder="Type to search cities"
														        name="wintType1">
																<option value="Banquet">Access and Control System</option>
																<option value="Fund Raiser">Audio visual Euipment</option>
																<option value="Dinner Party">Electrical Works</option>
														</select>
												</div>
												<div className="form-group">
														<label htmlFor="wint1">Product Sub-catagory :</label>
														<input type="text" className="form-control required"
														       id="wint1" /></div>
												<div className="form-group">
														<label htmlFor="wLocation1">Contract Type :</label>
														<select className="custom-select form-control required"
														        id="wLocation1" name="wlocation">
																<option value="">Select </option>
																<option value="India">Tender</option>
																<option value="USA">Rate Contract</option>
														</select>
												</div>
										</div>
										<div className="col-md-6">
												<div className="form-group">
														<label htmlFor="wjobTitle2">Tender Value :</label>
														<select className="custom-select form-control required"
														        id="wLocation1" name="wlocation">
																<option value="">Select </option>
																<option value="India">INR</option>
														</select>
														
												</div>
												<div className="form-group">
														<label htmlFor="wint1">Delivery Period in days :</label>
														<input type="text" className="form-control required"
														       id="wint1" /></div>
												<div className="form-group">
														<label htmlFor="wjobTitle2">Location :</label>
														<select className="custom-select form-control required"
														        id="wLocation1" name="wlocation">
																<option value="">Select City</option>
																<option value="India">India</option>
																<option value="USA">USA</option>
																<option value="Dubai">Dubai</option>
														</select>
														
												</div>	
												<div className="form-group">
														<label htmlFor="wint1">pincode :</label>
														<input type="text" className="form-control required"
														       id="wint1" /></div>	
												<div className="form-group">
														<label htmlFor="wjobTitle2">Tenderer class :</label>
														<select className="custom-select form-control required"
														        id="wLocation1" name="wlocation">
																<option value="">Select</option>
																<option value="India">A</option>
																<option value="USA">B</option>
																<option value="Dubai">others</option>
														</select>
														
												</div>				   	   
												
										</div>
								</div>
						</section>
		);
};

export default StepThree;