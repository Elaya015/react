import React, { Component } from 'react';

class StepTwo extends Component {

		state = {
				company: '',
				company_url: '',
				company_description: '',
		};

		handleChangeInput = ( event ) => {
				this.setState( {
						[event.target.name]: event.target.value,
				} );
		};

		render() {
				return (
						<div>
								<section>
										<div className="row">
												
												
												<div className="col-md-12">
														<div className="form-group">
																<label htmlFor="shortDescription3">Document Description :</label>
																<textarea name="document_description"
																          rows="6" className="form-control" onChange={ this.handleChangeInput }
																          value={ this.state.company_description } />
														</div>
												</div>
												<div className="col-md-6">
										<div className="form-group">
										<label htmlFor="wlocation2">Document Type :
														<span className="danger">*</span>
												</label>
												<select className="custom-select form-control required"
												        id="wlocation2" name="location">
														<option value="">-select-</option>
														<option value="pdf ">.pdf</option>
														<option value="xls">.xls</option>
														<option value="jpg">.jpg</option>
														
												</select></div>
								</div>
										</div>
								</section>
						</div>
				);
		}
}

export default StepTwo;