import React from 'react';

const StepOne = () => {

		return (
				<section>
						<div className="row">
								<div className="col-md-6">
										<div className="form-group">
												<label htmlFor="wfirstName2">Tender Reference Number :
														<span className="danger">*</span>
												</label>
												<input type="text" className="form-control required"
												       id="wfirstName2" name="firstName" required /></div>
								</div>
								<div className="col-md-6">
										<div className="form-group">
										<label htmlFor="wlocation2"> Tender Type :
														<span className="danger">*</span>
												</label>
												<select className="custom-select form-control required"
												        id="wlocation2" name="location">
														<option value="">Select Type</option>
														<option value="open Tender">Open Tender</option>
														<option value="Limited">Limited</option>
														<option value="EOI">EOI</option>
														<option value="AUCTION">AUCTION</option>
														<option value="SINGLE">SINGLE</option>
														<option value="Open">Open Limited</option>
												</select></div>
								</div>
						</div>
						<div className="row">
								<div className="col-md-6">
										<div className="form-group">
										<label htmlFor="wlocation2"> Form of Contract :
														<span className="danger">*</span>
												</label>
												<select className="custom-select form-control required"
												        id="wlocation2" name="location">
														<option value="">Select Type</option>
														<option value="Piece Work">Piece Work</option>
														<option value="Lump-sum">Lump-sum</option>
														<option value="Multi Stage">Multi Stage</option>
														
												</select></div>
								</div>
								<div className="col-md-6">
										<div className="form-group">
										<label htmlFor="wlocation2"> No Of Covers :
														<span className="danger">*</span>
												</label>
												<select className="custom-select form-control required"
												        id="wlocation2" name="location">
														<option value="">Select Type</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														
												</select>										</div>
								</div>
						</div>
						<div className="row">
								<div className="col-md-6">
										<div className="form-group">
												<label htmlFor="wlocation2"> Tender Catagory :
														<span className="danger">*</span>
												</label>
												<select className="custom-select form-control required"
												        id="wlocation2" name="location">
														<option value="">Select Type</option>
														<option value="Goods">Goods</option>
														<option value="Services">Services</option>
														<option value="Works">Works</option>
												</select>
										</div>
								</div>
								<div className="col-md-6">
										<div className="form-group">
												<label htmlFor="wdate2">Date of Birth :</label>
												<input type="date" className="form-control" id="wdate2" /></div>
								</div>
						</div>
				</section>
		);
};

export default StepOne;